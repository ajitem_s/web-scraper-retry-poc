from boto3 import client
import boto3
from botocore.config import Config
import botocore
import json


def hello(event, context):
    print("boto3 version:" + boto3.__version__)
    print("botocore version:" + botocore.__version__)

    message = 'Event: Key1 - {} | Key2 - {} | Key3 - {}'.format(event['key1'], event['key2'], event['key4'])

    return {
        'message': message
    }


def caller_async(event, context):
    config = Config(retries={'max_attempts': 5})
    lambda_client = client('lambda', config=config)

    # Found this a solution for an older version of boto library [link = https://github.com/boto/boto3/issues/1104#issuecomment-305136266]
    # This setting has no effect on the number of retries. The lambda still retries twice before sending error to DLQ.
    # lambda_client.meta.events._unique_id_handlers['retry-config-lambda']['handler']._checker.__dict__['_max_attempts'] = 0

    lambda_client.invoke(
        FunctionName='python-dlq-test-dev-hello',
        InvocationType='Event',
        Payload=json.dumps(event)
    )


def caller_sync(event, context):
    lambda_client = client('lambda')

    hello_res = lambda_client.invoke(
        FunctionName='python-dlq-test-dev-hello',
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json.dumps(event)
    )

    response = json.loads(hello_res['Payload'].read())

    print(response)

