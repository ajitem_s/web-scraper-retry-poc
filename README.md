Retry Mechanism

The aim of this POC is to identify a way to identify an error that may occur during or before an execution of an AWS Lambda. This error will then be processed and then the corresponding function shall be retried X number of times as defined by the user before erroring out.

There are two scenarios:

* **Synchronous Execution**
    
    For sync execution, the lambda client returns us the error in JSON format like below:
    
    ```json
    {
            "errorMessage": "'key4'", 
            "errorType": "KeyError", 
            "stackTrace": [
                    [
                        "/var/task/handler.py",
                        6,
                        "hello", 
                        "message = 'Event: Key1 - {} | Key2 - {} | Key3 - {}'.format(event['key1'], event['key2'], event['key4'])"
                    ]
            ]
    }
    ```
    
    We can use this JSON to identify the error type, the exact message returned and the trace. Upon receiving this error, we may chose to re-invoke the same lambda while incrementing the max_retry counter and sending this retry counter along with the original event payload to the new function instance.
* **Asynchronous Execution**
    
    For async execution, we are going to use the DLQ (Dead Letter Queues). The concept here is that, if an asynchronously executed lambda function fails, the AWS client retries the function twice before erroring out. This error message is sent to a queue that is known as a DLQ. This queue will contain a message with the original event payload and attributes containing error details.
    
    We can write another lambda that will be triggered by an input in the DLQ to identify and process the errors.
    
    **NOTE**
    
    AWS has recently added the provision of configuring this max_retry parameter. I have tried the following code ([refer](https://github.com/boto/boto3/issues/1104#issuecomment-305136266)):
    
    ```python
    lambda_client.meta.events._unique_id_handlers['retry-config-lambda']['handler']._checker.__dict__['_max_attempts'] = 5
    ```
    
    But this is no longer working. I would also not like to use this method as it involves accessing private members of the SDK which can change at any point of time.
    
    The other solution was released recently and can be found in the newer versions of boto3 and botocore. Using this, we can configure the client to accept max number of retries as follows:
    
    ```python
    config = Config(retries={'max_attempts': 5})
    lambda_client = client('lambda', config=config)
    ```
    
    This approach should work but it cannot be tested as AWS Lambda environment uses an older version of the boto and botocore libraries.
  